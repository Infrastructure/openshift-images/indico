#!/bin/bash

for path in /opt/indico/data/{archive,custom}; do
    [[ ! -d $path ]] && mkdir -p $path
done

timeout 1h indico celery worker -B --concurrency 2
