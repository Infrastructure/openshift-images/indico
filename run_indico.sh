#!/bin/bash

for path in /opt/indico/data/{archive,custom}; do
    [[ ! -d $path ]] && mkdir -p $path
done

indico db prepare
indico db upgrade
indico db --all-plugins upgrade

echo 'Starting Indico...'
uwsgi /etc/uwsgi.ini
