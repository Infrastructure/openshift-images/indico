FROM registry.redhat.io/rhel9/python-312:1

USER root
RUN dnf install -y pango

RUN pip install uwsgi python-ldap setuptools

RUN pip install --ignore-requires-python indico>=3.3.2 indico-plugins>=3.3 \
        git+https://gitlab.gnome.org/Infrastructure/indico-plugin-stripe

RUN ["/bin/bash", "-c", "mkdir -p --mode=775 /opt/indico/{etc,tmp,log,cache,data}"]

RUN git clone --depth=1 https://gitlab.gnome.org/Teams/Engagement/Events/gnome-indico-resources /opt/indico/custom && \
    git clone --depth=1 https://gitlab.gnome.org/Teams/Engagement/Events/gnome-indico-resources /usr/lib/python3.9/site-packages/indico/web/static/custom && \
    rm -rf /opt/indico/custom/.git /usr/lib/python3.9/site-packages/indico/web/static/custom/.git

ENV INDICO_CONFIG /opt/indico/etc/indico.conf
RUN indico setup create-symlinks /opt/indico && \
    indico setup create-logging-config /opt/indico/etc

COPY uwsgi.ini /etc/uwsgi.ini
COPY run_indico.sh run_celery.sh /opt/indico/
ADD ipa-ca.crt /etc/ipa-ca.crt
